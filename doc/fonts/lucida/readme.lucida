============================================================
                 TeX support files for the
           Lucida Bright and Lucida New Math fonts
                      as of 2008-05-06
============================================================
                                              Walter Schmidt
                                      w-a-schmidt(at)gmx.net


This distribution contains the TeX fonts metrics, virtual
fonts and LaTeX font definition files for the Lucida
Bright and Lucida New Math fonts.  

This file set does, however, NOT include:

* the actual Type 1 fonts (*.pfb, *.pfm, *.afm);

* LaTeX macro packages to support math typesetting.


The Lucida typeface family was designed by Charles Bigelow
and Kris Holmes.  
(R) Lucida is a trademark of Bigelow & Holmes Inc.
registered in the U.S. Patent & Trademark Office and other
jurisdictions.



Installing the Type 1 font files
--------------------------------
Make sure that the Type1 font files carry their original 
names, as distributed by Y&Y, PCTeX or TUG.  Do not rename 
them to standard ("Karl Berry style") names.

The following table shows the descriptive PostScript font name, the
original Y&Y l* name used for afm, pfb, and pfm files, and an h* tfm
name, for the Y&Y texnansi 8y encoding, in the case of the text fonts.
(The text fonts actually have several corresponding tfm's, one for each
TeX encoding used; notably, 8y can be replaced with 8t to get the LaTeX
T1 ("Cork") encoding.)
                                           
/FontName:                       .afm,pfb,pfm name:     sample tfm name:

LucidaBright                     lbr                    hlhr8y
LucidaBrightSmallcaps            lbrsc                  hlhrc8y
LucidaBright-Oblique             lbsl                   hlhro8y
LucidaBright-Italic              lbi                    hlhri8y
LucidaBright-Demi                lbd                    hlhb8y
LucidaBrightSmallcaps-Demi       lbdsc                  hlhbc8y
LucidaBright-DemiItalic          lbdi                   hlhbi8y
LucidaSans                       lsr                    hlsr8y
LucidaSans-Italic                lsi                    hlsri8y
LucidaSans-Demi                  lsd                    hlsb8y
LucidaSans-DemiItalic            lsdi                   hlsbi8y
LucidaSans-Bold                  lsb                    hlsu8y
LucidaSans-BoldItalic            lsbi                   hlsui8y
LucidaTypewriter                 lbtr                   hlcrt8y
LucidaTypewriterOblique          lbto                   hlcrot8y
LucidaTypewriterBold             lbtb                   hlcbt8y
LucidaTypewriterBoldOblique      lbtbo                  hlcbot8y
LucidaSans-Typewriter            lstr                   hlsrt8y
LucidaSans-TypewriterOblique     lsto                   hlsrot8y
LucidaSans-TypewriterBold        lstb                   hlsbt8y
LucidaSans-TypewriterBoldOblique lstbo                  hlsbot8y
LucidaCalligraphy-Italic         lbc                    hlcrie8y
LucidaHandwriting-Italic         lbh                    hlcriw8y
LucidaCasual                     lbkr                   hlcrn8y
LucidaCasual-Italic              lbki                   hlcrin8y
LucidaBlackletter                lbl                    hlcrf8y
LucidaFax                        lfr                    hlxr8y
LucidaFax-Italic                 lfi                    hlxri8y
LucidaFax-Demi                   lfd                    hlxb8y
LucidaFax-DemiItalic             lfdi                   hlxbi8y
LucidaNewMath-Arrows             lbma                   hlcra
LucidaNewMath-Arrows-Demi        lbmad                  hlcda
LucidaNewMath-Demibold           lbmd                   hlcdm
LucidaNewMath-DemiItalic         lbmdi                  hlcdim
LucidaNewMath-AltDemiItalic      lbmdo                  hlcdima
LucidaNewMath-Extension          lbme                   hlcrv
LucidaNewMath-Italic             lbmi                   hlcrim
LucidaNewMath-AltItalic          lbmo                   hlcrima
LucidaNewMath-Roman              lbmr                   hlcrm
LucidaNewMath-Symbol             lbms                   hlcry
LucidaNewMath-Symbol-Demi        lbmsd                  hlcdy
    
Store the .pfb and .pfm files in the directory    
    
  <texmf>/fonts/type1/bh/lucida    
    
of your TeX system, and store the related .afm files in    
    
  <texmf>/fonts/afm/bh/lucida    
    
(With TeX systems other than VTeX, the directory names do    
not actually matter, as long as the .pfb files reside at    
least somewhere below texmf/fonts/type1.  With VTeX/Free,    
however, you must use exactly the directory names as    
indicated above; otherwise the font map (.ali) file needs to    
be modified.)    
    
   
    
Unpacking the distribution    
--------------------------    
Unpack the ZIP archive lucida.zip in the directory <texmf>    
of your TeX system.  All files will then be generated in the    
appropriate directories.    
    
    
    
Installing the font map files    
------------------------------    
The following fonts map files are provided:
    
  lucida.map      to be used with dvips and pdfTeX    
  lucida.ali      to be used with VTeX/Free    
    
Copy the file you want to use to a suitable directory of
your TeX system; configure your TeX system so that it will
make use of this map file.  Consult the documentation of
your TeX system for how to do so!
    
    
    
Using the Lucida fonts with LaTeX    
---------------------------------    
The file texmf/doc/fonts/lucida/lucida.txt includes some    
usage notes and a list of all available font families,    
series and shapes with their NFSS classification.    
    
    
    
How this collection was created    
-------------------------------    
The font metrics, virtual fonts and .fd files for the T1 and
TS1 encodings were created using fontinst 1.928.  
The sources are available as ZIP archive file
lucida-fontinst.zip; note that there is no need to unpack
and install it if you only want to _use_ the fonts!
    
The font metric files for the LY1 encoded text fonts, as
well as the metrics for the math fonts, are those that were
formerly distributed by Y&Y.



License
-------
Copyright 2006  Walter Schmidt

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version
1.3 of this license or (at your option) any later version.

The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.
The Current Maintainer of this work is Walter Schmidt.

This work consists of all files disributed through the CTAN
directory fonts/metrics/bh/lucida/.



History    
-------    
2008-05-06
   
  license (LPPL) added

2006-03-23

  Include tfm names too.

2005-11-14

  Editorial/English changes.

2005-09-15

  Font family hlhj with oldstyle figures added.
  Documentation (doc/fonts/lucida/lucida.txt) updated
  accordingly.
   
2005-08-24    
    
  Changed directory layout:    
  Metrics, VFs and Type1 fonts are no longer spread over
  several directories each, and the documentation resides in
  <texmf>/doc/fonts/lucida now.  Thus, all bottom-level
  directories have the name "lucida" (which works well with
  TeX Live, although it is not required by the TDS.)
    
  The font map files lucidabr.map and lumath.map have been
  concatenated to one single file named "lucida.map"; and
  there is only one single map file "lucida.ali" for VTeX,
  too.
    
  Map files using standard ("Karl Berry style") names for    
  the Type1 font files have been abolished, since both    
  MikTeX and TeX Live seem to prefer the original file 
  names.
  (Note, however, that all tfm and vf files carry _standard_    
  names, because the original names would not be able to    
  distinguish between LY1, T1 and TS1 encoding.)    
    
  \texteuro is now provided in _all_ TS1-encoded fonts; when    
  there is no such character in the physical Type1 font, it    
  is faked.    
    
  Improved Polish diacritics and \dj in most virtual fonts.    
    
  Revised VFs and metrics of the smallcaps fonts hlhbc8t and    
  hlhrc8t to fix a minor bug regarding the size of the    
  hungarumlaut accent.    
  
  Oldstyle figures from the Lucida Calligraphy-Italic fonts
  are made available also for the italic shape of the Lucida
  Bright family.
    
  Revised documentation; in particular, the API provided by
  the fd files to scale the fonts (\DeclareLucidaFontShape)
  is now documented.
  
2003-03-12    
    
  All typewriter fonts have vertically centered asterisks    
  now; all textcompanion fonts have vertically centered    
  \textaskeriskcentered symbols.    
    
  The Omega symbol \textohm is made available in the    
  textcompanion fonts.    
    
  The Polish diacritics have been somewhat improved, but    
  are not really satisfactory yet.    
    
  Font mapping files have been moved out of the zip archive,    
  with respect to their changed location in the future TDS    
  1.0.    
    
  Some empty and unneeded directories have been removed    
  from the archive lucida.zip.    
    
2003-01-13    
  There are thorough changes over the previous CTAN distrib:    
    
  The new file set includes also the metrics for LY1 encoded    
  text fonts.  These are the original files from Y&Y, only    
  renamed to Karl-Berry-style names.  The related .fd files    
  were changed appropriately.  Thus, LY1 can be used easily    
  beside T1/TS1.    
    
  The LY1 and TS1 encodings make the Euro symbol (\texteuro)    
  accessible, which is present in the current release of the    
  Lucida fonts.    
    
  With T1/TS1 encoding, the same set of font series and    
  shapes is supported as with Y&Y's original distribution.    
  The previous CTAN distribution, in contrast, included a    
  number of additional series/shapes, which were based on    
  artificially compressed, expanded or slanted fonts and    
  faked smallcaps.  Notice that these were never documented!    
    
  The obsolete OT1 font encoding is no longer supported with    
  the Lucida fonts.    
    
  T1 encoded smallcaps fonts (hlhrc8t, hlhbc8t) include    
  normal digits now, as expected.  Oldstyle digits are still    
  available in the TS1 encoding.    
